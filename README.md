# publish

A ci pipeline to publish content to a sftp share, e.g. a web server.
Use cases:

* Static web page
* Reveal js presentation

## Use it in your gitlab ci

**Step 1**: Edit / create `.gitlab-ci.yml` so it includes the publish
template and contains a job generating / declarating content.

Minimal example:

`.gitlab-ci.yml`:

```yml
---
include:
  - project: 'sis/ci-pipelines/publish'
    file: 'publish.yml'

generate_content:
  stage: build
  script:
    - mkdir public/
    - echo "Chuck Norris counted to infinity - twice!" > public/index.html
  artifacts:
    paths:
      - public/
```

ℹ️ The publish job included by `sis/ci-pipelines/publish/publish.yml` assumes an
[artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)
folder named `public/`
(this is consistent with [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html#specify-the-public-directory-for-artifacts)),
available before the `deploy`
[stage](https://docs.gitlab.com/ee/ci/yaml/#stages). In the above
example, `build/` is created in the `generate_content` job which is
run during the `build` stage which comes before the `deploy`
stage. Also shown in the above example is how `generate_content`
exports the `public` folder as an artifact.

**Step 2**: Add a ssh key and the destination sftp/ssh URL
to your CI variables in the projects CI
settings.

First create a dedicated ssh key pair:

```sh
ssh-keygen -t ed25519 -C "gitlab-ci" -f gitlab-ci
```

Add the public key (`gitlab-ci.pub`) to `~/.ssh/authorized_keys` of your web server.

Then set the following CI variables (*Stettings* -> *CI/CD* -> *Variables*):

* `PUBLISH_KEY`: private ssh key (`gitlab-ci` gerated by the above
  `ssh-keygen` command)
* `PUBLISH_URL`: The destination in the form `user@host:/directory`

⚠️ Only expose those variables on protected branches. Otherwise all
developers creating a feature branch on the project will potentially
have access to the credentials.

ℹ️ The pipeline will only run on protected branches. If you create a
protected branch different from the default branch, `/branchname` will
be automatically appended to `PUBLISH_URL`. However in such a case,
you have to delete the subfolder manually on the destination once it is
no longer needed.
